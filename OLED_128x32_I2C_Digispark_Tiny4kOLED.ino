#include <Tiny4kOLED.h>

#define ERROR_LED_PIN 1
#define RST_PIN 4
int row = 0;
int mlx90614_i2cAddress = 0x5A;
const int REG_T_AMBIENT = 0x6;
const int REG_T_OBJECT  = 0x7;
const int DEBUG = 0;

void ResetOLED()
{
    pinMode(RST_PIN, OUTPUT);
    digitalWrite(RST_PIN, HIGH);
    delay(1);                   // VDD goes high at start, pause for 1 ms
    digitalWrite(RST_PIN, LOW);  // Bring reset low
    delay(10);                  // Wait 10 ms
    digitalWrite(RST_PIN, HIGH); // Bring out of reset
}

void BlinkErrorLed(uint32_t blinkTime, uint32_t repeat = 500)
{
  uint32_t offTime = blinkTime/2;
  for(uint32_t i = repeat; i > 0; i--)
  {
    digitalWrite(ERROR_LED_PIN, HIGH);
    delay(offTime);
    digitalWrite(ERROR_LED_PIN, LOW);
    delay(offTime);
  }
}

void setup() {
  pinMode(ERROR_LED_PIN, OUTPUT);
  // Send the initialization sequence to the oled. This leaves the display turned off
  oled.begin();
  oled.setFont(FONT6X8);
  // Clear the memory before turning on the display
  oled.clear();
  oled.on();
  // Switch the half of RAM that we are writing to, to be the half that is non currently displayed
  oled.switchRenderFrame();
  analogReference(INTERNAL1V1);
}

float GetInternalTemp() {
   int rawKelvin = analogRead(A0+15);
   int tempC = rawKelvin - 273;
   return (float)tempC;
}

float GetMLX90614Temp(uint8_t tempReg, uint32_t &errorCode) {
  uint8_t numBytesToRead = 3;
  TinyWireM.beginTransmission(mlx90614_i2cAddress);
  TinyWireM.write(tempReg); // Move slave register to point to 0x7
  TinyWireM.endTransmission(0); // Send write to slave, but don't stop communications (0)

  errorCode |= (TinyWireM.requestFrom(mlx90614_i2cAddress, numBytesToRead) << 8*1); // Read data to internal TinyWireM buffers
  char loB = (TinyWireM.receive() & 0xFF);
  char hiB = (TinyWireM.receive() & 0xFF);
  char PacketErrorCode = (TinyWireM.receive() && 0xFF); // Ignore PEC
  TinyWireM.endTransmission();

  double tempFactor = 0.02; // 0.02 degrees per LSB (measurement resolution of the MLX90614)
  double tempData = 0x0000;

  // Temperature measurement comprises of two bytes: [ xbbb bbbb | bbbb bbbb ], where b indicates meaningful bit
  tempData = (double)(((hiB & 0x007F) << 8) + loB);
  tempData = (tempData * tempFactor)-0.01;

  float celcius = tempData - 273.15;
  return celcius;
}

void loop() {
  UpdateDisplay();
  BlinkErrorLed(500,1); // Blink led as a sign of activity
  delay(10);
}

void UpdateDisplay() {
  // Clear the half of memory not currently being displayed.
  uint32_t eCode = 0;
  oled.clear();
  // Position the text cursor
  // In order to keep the library size small, text can only be positioned
  // with the top of the font aligned with one of the four 8 bit high RAM pages.
  // The Y value therefore can only have the value 0, 1, 2, or 3.
  // usage: oled.setCursor(X IN PIXELS, Y IN ROWS OF 8 PIXELS STARTING WITH 0);
  oled.setCursor(0, 0);
  oled.print("T_ambient: ");
  oled.print(GetMLX90614Temp(REG_T_AMBIENT, eCode));
  oled.print("C");
  oled.setCursor(0, 1);
  oled.print("T_object:  ");
  oled.print(GetMLX90614Temp(REG_T_OBJECT, eCode));
  oled.print("C");
  oled.setCursor(0, 2);
  oled.print("T_outisde: ");
  oled.print(GetInternalTemp());
  oled.print("C");

  if (DEBUG)
  {
    oled.print(" 0:");
    oled.print((eCode >> (8*0)) & 0xFF);
    oled.print(" 1:");
    oled.print((eCode >> (8*1)) & 0xFF);
    oled.print(" 2:");
    oled.print((eCode >> (8*2)) & 0xFF);
    oled.print(" 3:");
    oled.print((eCode >> (8*3)) & 0xFF);
  }

  // Swap which half of RAM is being written to, and which half is being displayed.
  // This is equivalent to calling both switchRenderFrame and switchDisplayFrame.
  oled.switchFrame();
}
