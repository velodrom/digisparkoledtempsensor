# README #

This Arduino based project uses three simple components to measure and display temperature values on a small 128x32 bit OLED screen.

Goal of the project is to monitor the temperature of a baby in a crib or a stroller. The device should be:

- small 
- lightweight 
- detachable 
- battery powered

To fulfill these requirements for the main CPU, the Attiny85 Digispark board was chosen for its small form factor and IO capabilities. The temperature sensor and OLED screen was chosen purely based on available components in my box of prototyping components.

The application is written for Arduino compatible Attiny85 16MHz Digispark board. Due to the limited program memory of Attiny85, instead of Adafruit GFX library we're using a lightweight I2C communiation library TinyWireM from Adafruit, and an OLED graphics library from Tiny4KOLED.
Temperature measurements are collected from both an external IR sensor and an onboard temperature sensor. The external sensor is connected to the main CPU and display unit with ~1m silicon wire I2C connection (4 wires: VIN, GND, SDA, SCL), allowing flexible sensor and display placement.

#### Hardware: ####

* Attiny85 Digispark 16MHz board
* MLX90614ESF I2C 5V IR temperature sensor
* SSD1306 128x32 double buffered I2C OLED screen on a ADXL335 breakout board
* 2x10kOhm pull-up resistors for SDA and SCL lines
* Bunch of wires

### External dependencies ###

* Tiny4kOLED library: https://github.com/datacute/Tiny4kOLED
* TinyWireM I2C library: https://github.com/adafruit/TinyWireM
